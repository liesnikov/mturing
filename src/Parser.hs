module Parser (parseAll, parseHeader, parseInstructions,
               Instruction,
               State, Cell,
               Tape (..), Movement (..)) where

import Data.Either (rights)
import Data.Char (toLower)
import Control.Monad (liftM, void)

import Text.ParserCombinators.Parsec (Parser, parse,
                                      (<|>), (<?>), try,
                                      manyTill, between,
                                      oneOf, optional, lookAhead,
                                      many, many1,
                                      char, string,
                                      eof)
import Text.Parsec.Char (endOfLine, anyChar, alphaNum,
                         space, tab)
import Machine (Tape (..), Movement (..))

type Comment = String
type State = String
type Cell = String

toTape :: [c] -> c -> [c] -> Tape c
toTape l c r = Tape {lft = l,
                     crnt = c,
                     rght = r}

type Pair = (State, Cell)
type Instruction = (Pair, (Pair, Movement))

parseAll = parse $ (,) <$> (header <* ((many $ oneOf "#-=") >> endOfLine)) <*> insts

parseInstructions = parse insts
parseHeader = parse header

header = (,) <$> (start_state <* endOfLine) <*> ((start_tape endOfLine) <* endOfLine)
         where
          start_state = p_strict_state
          reg_cell = p_strict_cell
          chosen_cell = (string "[") *> ( p_strict_cell <* (string "]") )

          start_tape :: Parser a -> Parser (Tape Cell)
          start_tape end = toTape <$> manyTill (reg_cell <* many1 fsep) (try.lookAhead $ chosen_cell)
                                  <*> (chosen_cell <* many1 fsep)
                                  <*> ((++) <$> (manyTill (reg_cell <* many1 fsep)
                                                          (try.lookAhead $ reg_cell <* end))
                                            <*> ((:[]) <$> reg_cell))

insts :: Parser [Instruction]
insts = rights <$> ((++)
                    <$> manyTill (instline <* many1 csep) (try.lookAhead $ lastline)
                    <*> ((:[]) <$> lastline))

instline :: Parser (Either Comment Instruction)
instline = try (liftM (Left) cmnt)
            <|> liftM (Right) cmnd
            <?> "either a comment or a command"

lastline :: Parser (Either Comment Instruction)
lastline = instline <* ((many csep) *> eof)

cmnt :: Parser String
cmnt = (char '#') *> manyTill anyChar (try.void.lookAhead $ endOfLine <* optional eof) <?> "endOfLine at the end of the comment"

cmnd :: Parser Instruction
cmnd = ((,) <$> p_left <* lrsep <*> p_right) <?> "command format is state1 cell1 -> state2 cell2 movement"

p_pair :: Parser Pair
p_left :: Parser Pair
p_right :: Parser (Pair, Movement)

p_pair = (,) <$> p_state <*> (fsep *> p_cell)
p_left = p_pair
p_right = (,) <$> p_pair <* fsep <*> p_movement

p_movement :: Parser Movement
p_movement = (do
  c <- oneOf "lLrRhH"
  return $ case toLower c of
            'l' -> MoveLeft
            'h' -> Hold
            'r' -> MoveRight
              ) <?> "one of l/r/h in the movement field"

p_state :: Parser State
p_strict_state :: Parser State
p_cell :: Parser Cell
p_strict_cell :: Parser Cell

p_state = (try p_strict_state) <|> string "*"
p_strict_state = many1 $ alphaNum <|> oneOf "_-"
p_cell = p_state
p_strict_cell = p_strict_state

lrsep :: Parser ()
fsep :: Parser ()
csep :: Parser ()

lrsep = void (many1 fsep >> optional (string "->" >> many1 fsep )) <?> "separator between left and right side"
fsep  = void (space <|> tab) <?> "separator between fields"
csep  = void ((try (char ';' >> endOfLine)) <|> (char ';') <|> endOfLine) <?> "command separator or end of file"
