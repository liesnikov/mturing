module Translator (translatewh, translatei,
                   fins, flookup, flookup',
                   transMap, M.fromList) where

import qualified Parser
  as Raw (Instruction, State, Cell, Tape (..), Movement(..))

import Machine (Instruction, InstructionSet,
                State, Cell,
                Wildcard (Wildcard), Movement (..))

import qualified Data.Set as S
import qualified Data.Map as M

translatewh :: ((Raw.State, Raw.Tape Raw.Cell), [Raw.Instruction]) ->
                (M.Map State Raw.State, M.Map Cell Raw.Cell)
translatewh ((ss, t), l) = let
                            ci = collectInstruction

                            (istateSet, icellSet) = foldl ci (S.empty, S.empty) l

                            toList t = Raw.lft t ++ [Raw.crnt t] ++ Raw.rght t

                            stateSet :: S.Set Raw.State
                            stateSet = S.insert ss istateSet
                            cellSet :: S.Set Raw.Cell
                            cellSet = (foldl (flip S.insert) icellSet (toList t))

                            (sl, cl) = (zip           [0..]  $ S.toList stateSet,
                                        zip (map Just [0..]) $ S.toList cellSet)
                            (s, c) = (M.fromList sl,
                                      M.fromList cl)
                           in
                            (s, c)

translatei :: [Raw.Instruction] -> (M.Map State Raw.State, M.Map Cell Raw.Cell)
translatei l = let
                 ci = collectInstruction
                 (stateSet, cellSet) = foldl ci (S.empty, S.empty) l
                 (sl, cl) = (zip           [0..]  $ S.toList stateSet,
                             zip (map Just [0..]) $ S.toList cellSet)
                 (s, c) = (M.fromList sl,
                           M.fromList cl)
               in
                 (s, c)

collectInstruction :: (S.Set Raw.State, S.Set Raw.Cell) ->
                      Raw.Instruction ->
                      (S.Set Raw.State, S.Set Raw.Cell)
collectInstruction acc n = let -- helper function to fold over instruction list collecting states/cells into set, filtering wildcards
                            (s, c) = acc
                            ((s1, c1), (s2, c2)) = (fst n, fst . snd $ n)
                            eqa = \x -> x /= "*"
                            ns = filter eqa [s1, s2]
                            nc = filter eqa [c1, c2]
                           in
                            (foldl (flip S.insert) s ns, foldl (flip S.insert) c nc)

flookup' :: Ord k => M.Map k b -> k -> Either Wildcard b
flookup' m x = maybe (Left Wildcard) (Right) $ M.lookup x m -- translate maybe, if nothing found assume wildcard

flookup :: Ord k => M.Map k b -> k -> Maybe b
flookup = flip M.lookup

transMap :: (Ord k, Ord a) => M.Map k a -> M.Map a k
transMap = M.fromList . map swap . M.toList
          where swap (a,b) = (b,a)

-- poor man's `bifunctor' for instruction
fins :: M.Map Raw.State State -> M.Map Raw.Cell Cell ->
        Raw.Instruction -> Instruction
fins sMap cMap ri = let
                      l = flookup' -- assuming that if raw.something isn't in map, it's a wildcard, i.e. hacking shit together
                      ((ls, lc), ((rs, rc), m)) = ri -- get data from raw.instruction
                    in
                      ( (l sMap ls, l cMap lc),
                       ((l sMap rs, l cMap rc), m))
