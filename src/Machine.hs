{-# LANGUAGE DeriveFunctor #-}

module Machine (State, Movement(..), Cell,
                Wildcard(..),
                Instruction, InstructionSet,
                Tape(..), run) where

import qualified Data.Map as M
import qualified Control.Monad.State as St

import Data.Maybe (listToMaybe,
                   catMaybes)

type State = Integer
data Movement = MoveLeft | Hold | MoveRight deriving (Eq, Ord, Show)
type Cell = Maybe Integer

data Tape c = Tape {lft :: [c],-- left
                    crnt ::  c, -- current
                    rght :: [c] -- right
                    } deriving (Show, Functor)
type MachinePair = (State, Cell)

data Wildcard = Wildcard deriving (Eq, Ord, Show)
type InstructionCell = Either Wildcard Cell
type InstructionState = Either Wildcard State

type InstructionPair = (InstructionState, InstructionCell)
type Instruction = (InstructionPair, (InstructionPair, Movement))

type InstructionSet = M.Map InstructionPair
                            (InstructionPair, Movement)

type Continue = Bool
data Machine = M {tp       :: Tape Cell,
                  stt      :: State,
                  instst   :: InstructionSet}

find :: InstructionSet -> MachinePair -> Maybe (MachinePair, Movement)
find im (s, c) = let
                  rw x = case fst x of
                          (Right ns, Right nc)           -> Just ((ns, nc), snd x)
                          (Right ns, Left Wildcard)      -> Just ((ns,  c), snd x)
                          (Left Wildcard, Right nc)      -> Just ((s , nc), snd x)
                          (Left Wildcard, Left Wildcard) -> Just ((s ,  c), snd x)
                  priority = [(Right s      , Right c ),
                              (Right s      , Left Wildcard),
                              (Left Wildcard, Right c ),
                              (Left Wildcard, Left Wildcard)]
                 in
                  listToMaybe . catMaybes $ map (\k -> (M.lookup k im) >>= rw) priority

move :: Movement -> Tape Cell -> Tape Cell
move m t = let
            (ol, oc, or) = (lft t, crnt t, rght t)
            tail' x = case x of
                        []     -> Nothing
                        _ : ys -> Just ys
            head' x = case x of
                        []     -> Nothing
                        y : _ -> Just y
           in
            case m of
              MoveLeft  -> Tape {lft = maybe [] id (tail' ol),
                                 crnt = maybe Nothing id (head' ol),
                                 rght = oc : or}
              MoveRight -> Tape {lft = oc : ol,
                                 crnt = maybe Nothing id (head' or),
                                 rght = maybe [] id (tail' or)}
              Hold -> t

putc :: Cell -> Tape Cell -> Tape Cell
putc nc t = let
             (ol, or) = (lft t, rght t)
            in
             Tape {lft  = ol,
                   crnt = nc,
                   rght = or}

run :: InstructionSet -> State -> Tape Cell -> (Tape Cell, State)
run i s t = let
              e = M {tp = t,
                     stt = s,
                     instst = i}
              step :: (Continue, State) -> St.State Machine (Continue, State)
              step (c, s) = if c
                              then (do
                                     ms <- St.get
                                     let {(ot, os, is) = (tp ms, stt ms, instst ms)};
                                     case find is (os, crnt ot) of
                                       Just ((ns, nc), m) -> St.put M {tp     = move m $ putc nc ot,
                                                                       stt    = ns,
                                                                       instst = is} >> return (True, ns)
                                       Nothing            -> return (False, s)) >>= step
                              else return (c,s)
            in
              fst.flip St.runState e $ do
                                        m <- St.get
                                        (_, ns) <- step (True, stt m) -- iterate with `continue' set to true and previous state
                                        ms <- St.get -- get state (Machine)
                                        return (tp ms, ns)
