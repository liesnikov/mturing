{-# LANGUAGE DeriveDataTypeable #-}
module Main where

import qualified Machine as M

import Parser (parseAll)
import Translator (translatewh, flookup, flookup', fins, transMap)
import Control.Monad (join)
import qualified Data.Map as Map (fromList)

import System.Console.CmdArgs
  (Data, Typeable, def, args, cmdArgs, summary, typ, (&=))

data Config = Config
  { filename :: String
} deriving (Show, Data, Typeable)

config :: Config
config = Config
  {filename = def &= typ "FILE" &= args
  }
  &= summary "this is a turing machine emulator"

main :: IO ()
main = do
        c <- cmdArgs config
        file <- readFile $ filename c
        case parseAll (filename c) file of
          Left l               -> print l
          Right r@((rs, rst),ris)  -> do
                                      let {
                                        (revms, revmc) = translatewh r;
                                        (ms, mc) = (transMap revms, transMap revmc); -- Raw to regular
                                        tt = fmap (\x -> join $ flookup mc x) rst; -- translated tape
                                        is = Map.fromList $ fmap (fins ms mc) ris; --instruction set
                                        }
                                      print ms
                                      print mc
                                      print is
                                      print $ flookup ms rs
                                      print tt
                                      case flookup ms rs of
                                        Just ts -> print $ M.run is ts tt
                                        Nothing -> print "something's wrong"
